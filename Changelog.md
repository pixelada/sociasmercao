# CHANGELOG MYMODULE FOR <a href="https://www.dolibarr.org">DOLIBARR ERP CRM</a>

## 0.1.rc1 (2019/10/25)
+ Initial public testing version.
+ adherents managment with quotas.
+ adherents money movement managment for the market shop.
+ etc.

```
 Copyright (C) 2019 Pixelada S. Coop. And. <info(at)pixelada(dot)org)>
 
 This file is part of SociasMercao
 
 SociasMercao is free software; you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation; either version 3 of the
 License, or (at your option) any later version.
 
 SociasMercao is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with SociasMercao. If not, see <http://www.gnu.org/licenses/>.
```