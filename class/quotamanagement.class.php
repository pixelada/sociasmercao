<?php
/* Copyright (C) 2017  Laurent Destailleur <eldy@users.sourceforge.net>
 * Copyright (C) 2019 Pixelada S. Coop. And. <info(at)pixelada(dot)org>
 *
 * This file is part of SociasMercao
 *
 * SociasMercao is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation; either version 3 of the 
 * License, or (at your option) any later version.
 *
 * SociasMercao is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with SociasMercao. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file        /class/quotamanagement.class.php
 * \ingroup     sociasmercao
 * \brief       class managing quotas of adherents, and status upgrading, etc.
 */

// Put here all includes required by your class file
//require_once DOL_DOCUMENT_ROOT . '/core/class/commonobject.class.php';
//require_once DOL_DOCUMENT_ROOT . '/societe/class/societe.class.php';
//require_once DOL_DOCUMENT_ROOT . '/product/class/product.class.php';
require_once DOL_DOCUMENT_ROOT . '/sociasmercao/class/quota.class.php';
require_once DOL_DOCUMENT_ROOT . '/sociasmercao/class/modadherents.class.php';
require_once DOL_DOCUMENT_ROOT . "/core/lib/admin.lib.php";

/**
 * Class for Quotas
 */
class QuotaManagement
{
    public $date_last_quota;
    
	/**
	 * Constructor
	 *
	 * 
	 */
	public function __construct()
	{
		global $conf, $langs, $user, $db;
        $this->date_last_quota = NULL;
	}
    
	/**
	 * Function for upgrading quota type of adherent
	 * @param modAdherent, my adherent modified class
	 *
	 * @return	int			0 if OK, <>0 if KO (this function is used also by cron so only 0 is OK)
	 */
	public function upgradingQuota($myModAdherent)
	{
		global $conf, $langs, $user, $db;
        $error = 1;

        if($myModAdherent->ref && ($myModAdherent->quota_freq || $myModAdherent->quota))
        {
            $myQuota = new quota($db);

            $result = $myQuota->fetchLast(NULL, $myModAdherent->ref);

            if(!$result) //no found
            {
                //adherent already has no quota, creating first
                if(!$myModAdherent->date_last_quota)
                {
                    $this->date_last_quota = date("Y-m-d H:i:s");
                    $myModAdherent->date_last_quota = $this->date_last_quota;
                }
                $myQuota->date_quota = $myModAdherent->date_last_quota;
                $myQuota->quota_freq = $myModAdherent->quota_freq;
                //partial-payment if we are in inter-periods
                $myQuota->quota = $this->calculatePartialCuota($myModAdherent->quota, $myQuota->quota_freq, $myQuota->date_quota);
                //misc
                $myQuota->ref = $myModAdherent->ref;

                $myQuota->create($user);
                $error = 0;
            }
            else //if last quota found
            {
                if($myQuota->status != 1) //if not paid yet, we can change it
                {
                    $myQuota->quota_freq = $myModAdherent->quota_freq;
                    $myQuota->quota = $this->calculatePartialCuota($myModAdherent->quota, $myQuota->quota_freq, $myQuota->date_quota);

                    $myQuota->update($user);
                    $error = 0;
                }
            }
        }
		return $error;
	}

    /**
	 * Function for calculating partial quota in itner-periods
	 * @param float quota, absolute quota value
     * @param float quota_freq, frequency of quota payments
     * @param string from_date, month as starting point of quota payment calculation
	 *
	 * @return	float new_quota			
	 */
	public function calculatePartialCuota($quota, $quota_freq, $from_date)
	{
        $new_quota = $quota;
        $month = date('m', $from_date);
        
        //every three months payment
        if($quota_freq == 1 &&  ($month === '2' || $month === '02' || $month === '5' || $month === '05' || $month === '8' || $month === '08' || $month === '11'))
            $new_quota = (float)($quota * 2.0) / 3.0;
        if($quota_freq == 1 &&  ($month === '3' || $month === '03' || $month === '6' || $month === '06' || $month === '9' || $month === '09' || $month === '12'))
            $new_quota = (float)($quota) / 3.0;
        //every six months payment
        if($quota_freq == 2 &&  ($month === '2' || $month === '02' || $month === '8' || $month === '08')) 
            $new_quota = (float)($quota * 5.0) / 6.0;
        if($quota_freq == 2 &&  ($month === '3' || $month === '03' || $month === '9' || $month === '09')) 
            $new_quota = (float)($quota * 4.0) / 6.0;
        if($quota_freq == 2 &&  ($month === '4' || $month === '04' || $month === '10')) 
            $new_quota = (float)($quota * 3.0) / 6.0;
        if($quota_freq == 2 &&  ($month === '5' || $month === '05' || $month === '11')) 
            $new_quota = (float)($quota * 2.0) / 6.0;
        if($quota_freq == 2 &&  ($month === '6' || $month === '06' || $month === '12')) 
            $new_quota = (float)($quota) / 6.0;
        //every year payment
        if($quota_freq == 3 &&  ($month === '2' || $month === '02'))
            $new_quota = (float)($quota * 11.0) / 12.0;
        if($quota_freq == 3 &&  ($month === '3' || $month === '03'))
            $new_quota = (float)($quota * 10.0) / 12.0;
        if($quota_freq == 3 &&  ($month === '4' || $month === '04'))
            $new_quota = (float)($quota * 9.0) / 12.0;
        if($quota_freq == 3 &&  ($month === '5' || $month === '05'))
            $new_quota = (float)($quota * 8.0) / 12.0;
        if($quota_freq == 3 &&  ($month === '6' || $month === '06'))
            $new_quota = (float)($quota * 7.0) / 12.0;
        if($quota_freq == 3 &&  ($month === '7' || $month === '07'))
            $new_quota = (float)($quota * 6.0) / 12.0;
        if($quota_freq == 3 &&  ($month === '8' || $month === '08'))
            $new_quota = (float)($quota * 5.0) / 12.0;
        if($quota_freq == 3 &&  ($month === '9' || $month === '09'))
            $new_quota = (float)($quota * 4.0) / 12.0;
        if($quota_freq == 3 &&  ($month === '10'))
            $new_quota = (float)($quota * 3.0) / 12.0;
        if($quota_freq == 3 &&  ($month === '11'))
            $new_quota = (float)($quota * 2.0) / 12.0;
        if($quota_freq == 3 &&  ($month === '12'))
            $new_quota = (float)($quota) / 12.0; 
        
        return $new_quota;
    }
    
	/**
	 * Action executed by scheduler
	 * CAN BE A CRON TASK. In such a case, paramerts come from the schedule job setup field 'Parameters'
	 *
	 * @return	int			0 if OK, <>0 if KO (this function is used also by cron so only 0 is OK)
	 */
	//public function doScheduledJob($param1, $param2, ...)
	public function doScheduledJob()
	{
		//to be executed every day
		global $conf, $langs, $db;
		$error = -1;
        $trimonth_array = array(1,4,7,10);
        $sexmonth_array = array(1,7);
        
		if($conf->global->cronJobQuota && $conf->global->cronJobQuotaLastDate) //if configured
		{
			$error = 1;
            $month = (int)date('m',strtotime($conf->global->cronJobQuotaLastDate));
			if((int)date('m') != $month) //if today is new month:
			{
               	dol_syslog(__METHOD__, LOG_DEBUG);
                //select all adherents ordered by quota_freq
                $tmpAdherent = new modAdherents($db);
                $my_adherents = $tmpAdherent->fetchAll('ASC', 'quota_freq', NULL, 0, array('status' => '>=0'));
                //debug tag for forcing quota upgrading
                $force_updating = TRUE;
                 
                //define wich quotas to upgrade
                dol_syslog('upgrading monthly quotas...yes!', LOG_DEBUG);
                if(in_array((int)date('m'), $trimonth_array)|| $force_updating)
                {
                    $update_3monthly_quotas = TRUE;
                }
                else
                {
                    $update_3monthly_quotas = FALSE;
                }
                dol_syslog('upgrading 3-monthly quotas...'.($update_3monthly_quotas?'yes!':'no!'), LOG_DEBUG);
                if(in_array((int)date('m'), $sexmonth_array)|| $force_updating)
                {
                    $update_6monthly_quotas = TRUE;
                }
                else
                {
                    $update_6monthly_quotas = FALSE;
                }
                dol_syslog('upgrading 6-monthly quotas...'.($update_6monthly_quotas?'yes!':'no!'), LOG_DEBUG);            
                if((int)date('m') == 1 || $force_updating)
                {
                    $update_yearly_quotas = TRUE;
                }
                else
                {
                    $update_yearly_quotas = FALSE;
                }
                dol_syslog('upgrading yearly quotas...'.($update_yearly_quotas?'yes!':'no!'), LOG_DEBUG);
                //quotas upgrading
                $error = $this->upgradeQuotas($my_adherents, $update_3monthly_quotas, $update_6monthly_quotas, $update_yearly_quotas);
                
                if($error == 0)
                {
                    $conf->global->cronJobQuotaLastDate = date("Y-m-d H:i:s");
                    dol_syslog('everything\'s ok, so upgrading last updating quotas\'s date to: '.$conf->global->cronJobQuotaLastDate, LOG_DEBUG);
                    dolibarr_set_const($db, 'cronJobQuotaLastDate', $conf->global->cronJobQuotaLastDate);
                }
            }                
        }
        if($error == 1)
            $this->output = 'we are not in month, comparing month from:'.$conf->global->cronJobQuotaLastDate.' (month='.$month.') ,and month from now:'.date().' (month='.(int)date('m').')';
        if($error == 0)
            $this->output = 'Done and ok!';
		return 0;
	}

    /**
	 * Function for upgrading adherents quotas
	 * @param array(modAdherent) array with all adherents to process, passed by reference as we modify it and cut it with all already procesed adherents removed.
     * @param bool upgrade 3 monthly quotas
     * @param bool upgrade 6 monthly quotas
     * @param bool upgrade yearly quotas
	 *
	 * @return  int error  0 ok,  <> 0 if error.
	 */
	public function upgradeQuotas(&$my_adherents, $updt3monthly, $updt6monthly, $updtyearly)
	{
		//to be executed every day
		global $conf, $langs, $db, $user;
        $error = 0;
        
        //go throw all modadherents
        foreach ($my_adherents as $index => $adherent)
        {
            if($adherent->quota_freq == 0)
            {
                dol_syslog('upgrading adherent:'.$adherent->ref.' ,id:'.$adherent->id.' ,do we upgrade it?...TRUE!', LOG_DEBUG);
            }
            //checking if we are upgrading monthly quotas or also 3monthly, 6monthly or yearly quotas
            if($adherent->quota_freq == 1)
            {
                dol_syslog('upgrading adherent:'.$adherent->ref.' ,id:'.$adherent->id.' ,do we upgrade it?...'.($updt3monthly == TRUE?'TRUE!':'FALSE!'), LOG_DEBUG);
                if($updt3monthly == FALSE)
                    break 1;
            }
            if($adherent->quota_freq == 2)
            {
                dol_syslog('upgrading adherent:'.$adherent->ref.' ,id:'.$adherent->id.' ,do we upgrade it?...'.($updt6monthly == TRUE?'TRUE!':'FALSE!'), LOG_DEBUG);
                if($updt6monthly == FALSE)
                    break 1;
            }
            if($adherent->quota_freq == 3)
            {
                dol_syslog('upgrading adherent:'.$adherent->ref.' ,id:'.$adherent->id.' ,do we upgrade it?...'.($updtyearly == TRUE?'TRUE!':'FALSE!'), LOG_DEBUG);
                if($updtyearly == FALSE)
                    break 1;
            }

            //getting las quota of adherent
            $myQuota = new quota($db);
            $result = $myQuota->fetchLast(NULL, $adherent->ref);
            if(!$result) //no found, modadherents has no quotas in database
            {
                //adherent already has no quota, creating list
                if($adherent->date_last_quota === NULL || $adherent->date_last_quota == 0)
                {
                    if($adherent->date_creation === NULL || $adherent->date_creation == 0)
                    {
                        $adherent->date_creation = strtotime('2016-12-01 01:01:01'); //TODO: what to do with a non creation date user?                        
                    }
                    $adherent->date_last_quota = $adherent->date_creation;
                }
                $num_quotas = $myQuota->createQuotas($adherent->date_last_quota, $adherent->quota_freq, $adherent->quota, $adherent->ref);
                if($num_quotas <>0)
                {
                    if($adherent->status <> -1)
                    {
                        if($num_quotas == 1)
                        {
                            $adherent->status = 0;
                        }
                        if($num_quotas > 1)
                        {
                            $adherent->status = 2;
                        }
                    }
                    $adherent->update($user); //TODO: in one sql transaction would be nicer
                }
            }
            else //if last quota found
            {
                if($myQuota->isActualPeriod())
                {
                    if(($myQuota->quota_freq != $adherent->quota_freq) || ($myQuota->quota != $adherent->quota)) //if it has been changed
                    {
                        if(!$myQuota->status) //if not paid at all
                        {
                            $myQuota->quota_freq = $adherent->quota_freq;
                            $myQuota->quota = $adherent->quota;
                            // $myQuota->update($user);
                        }                        
                    }
                    $myQuota->quota = $this->calculatePartialCuota($adherent->quota, $myQuota->quota_freq, $myQuota->date_payment);
                    $myQuota->update($user);
                }
                else
                {
                    $adherent->date_last_quota = $myQuota->date_quota;
                    $num_quotas = $myQuota->createQuotas($adherent->date_last_quota, $adherent->quota_freq, $adherent->quota, $adherent->ref);                       

                    if($num_quotas <>0)
                    {
                        if($adherent->status <> -1)
                        {
                            if($num_quotas == 1)
                            {
                                $adherent->status = 0;
                            }
                            if($num_quotas > 1)
                            {
                                $adherent->status = 2;
                            }
                        }
                        $adherent->update($user); //TODO: in one sql transaction would be nicer
                    }
                }
            }
            $result = NULL;
        }
        $my_adherents = $new_my_adherents;
        return $error;
	}
    /**
	 * Function for paying adherents quotas
	 * @param modadherent, adherent instance 
     * @param float payment
     * @param datetime payment_date
	 *
	 * @return  int error  0 ok,  <> 0 if error.
	 */
	public function payQuota(&$my_adherent, $amount, $date_payment)
	{
        global $user, $db;
        $error = -1;
        $amount_to_pay = $amount;

        do
        {
            //getting first unpaid quota of adherent
            $my_quota = new quota($db);
            $result = $my_quota->fetchFirstUnpaid(NULL, $my_adherent->ref);
        
            if($result)
            {
                // we pay it
                // whole quota payment
                if(($my_quota->amount + $amount_to_pay) >=  $my_quota->quota)
                {
                    $my_quota->amount = $my_quota->quota;
                    $my_quota->status = 2; // paid                    
                }
                // we almost pay it
                //partial quota payment
                else
                {
                    $my_quota->amount +=  $amount_to_pay;
                    $my_quota->status = 1;
                    $amount_to_pay = -1;
                }
                $my_quota->date_payment = $date_payment;
                $amount_to_pay = $amount_to_pay - (real)$my_quota->quota;
                if($my_quota->update($user))
                    $error = 0;
                
            }
            else
            {
                // extra?

                $my_quota->quota_freq = 10; //extra quota
                $my_quota->date_quota = $date_payment;
                $my_quota->date_payment = $date_payment;
                $my_quota->status = 2;
                $my_quota->amount = $my_quota->quota = $amount_to_pay;
                $my_quota->ref = $my_adherent->ref;
                $amount_to_pay = -1;
                if($my_quota->create($user))
                    $error = 0;

            }
        
        }
        while($amount_to_pay > 0);
        // if no error, and adherent not disabled
        if(!$error && $my_adherent->status >= 0)
        {
            $result = $my_quota->count('ref = '. $my_adherent->ref . ' AND status >=0 AND status < 2');
            //dol_syslog('DEBUG: number of registers counted: ' . $result, LOG_DEBUG);
            if($result == 0)            
                $my_adherent->status = 1;
            else if($result == 1)
                $my_adherent->status = 0;
            else
                $my_adherent->status = 2;
        }

        return $error;
    }
}
